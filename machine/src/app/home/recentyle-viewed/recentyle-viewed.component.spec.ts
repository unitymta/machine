import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentyleViewedComponent } from './recentyle-viewed.component';

describe('RecentyleViewedComponent', () => {
  let component: RecentyleViewedComponent;
  let fixture: ComponentFixture<RecentyleViewedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentyleViewedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentyleViewedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
