import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service/service.service';

@Component({
    selector: 'app-main-home',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainHomeComponent implements OnInit {
    products: any[];

    constructor(private productService: ServiceService) { }

    ngOnInit() {
        this.getProducts();
    }

    getProducts(): void {
        // console.log( this.productService.getProducts(), 'GGGG')
        this.productService.getProducts().subscribe(products => this.products = products);
        // console.log( this.products, 'GGGG')
    }

}
