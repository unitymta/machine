import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ServiceService } from '../../service/service.service';

@Component({
	selector: 'app-detail',
	templateUrl: './detail.component.html',
	styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {	

	@Input() product: any;

	constructor(private route: ActivatedRoute, private productService: ServiceService, private location: Location) { }

	ngOnInit() {		
		this.getProduct();
	}	
	
	getProduct(): void{
		let temp = this.route.snapshot.paramMap.get('code');
		console.log(temp,'TEMP');
		this.productService.getProducts().subscribe(product => {
			product.map(value => {
				let t: any = value['productCode'];
				if(t.toString() === temp){
					this.product = value;
				}
			});		
		});
	}
	
	saveProduct(): void {
		this.productService.updateProduct(this.product).subscribe(() => alert('update success'));
		this.location.back();
	}

	back(): void {
		this.location.back();
	}
}
