import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
	providedIn: 'root'
})
export class LoginDataService implements InMemoryDbService {

	createDb() {
		const login = [
			{ "userid": "sangtx", "password": "sang" },
			{ "userid": "sangtx2", "password": "123456" }
		];
		return { login };
	}
}
