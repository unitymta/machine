import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service/service.service';

@Component({
	selector: 'app-new-arrival',
	templateUrl: './new-arrival.component.html',
	styleUrls: ['./new-arrival.component.scss']
})
export class NewArrivalComponent implements OnInit {
	products: number[] = [];

	constructor(private productService: ServiceService) { }

	ngOnInit() {
		this.getProducts();
	}

	getProducts() {
		this.productService.getProducts().subscribe(products => {		
			// let sorrtedArray:Array<number> = products.sort((n1,n2)=> n1 > n2);
			let sorrtedArray: number[] = products.sort((a,b) => {			
				let dateA: any = new Date(a["lastAdd"]);
				let dateB: any = new Date(b["lastAdd"]);
    			return (dateA - dateB) ? 1 : 0;
			});
			sorrtedArray.reverse();
			return this.products = products;
		});
	}
}
