import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from '../../service/service.service';

@Component({
	selector: 'app-main-detail',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.scss']
})
export class MainDetailComponent implements OnInit {
	product: any;	
	countCart: number = 1;

	constructor(private route: ActivatedRoute,
				private productService: ServiceService) { }	

	ngOnInit() {
		this.getProduct();
	}

	getProduct(): void {
        let temp = this.route.snapshot.paramMap.get('code');
		this.productService.getProducts().subscribe(products => {					
			products.map(value => {
				let t: any = value["productCode"];
				if(t.toString() === temp){
					this.product = value;
				}
			})
		});	
	}
	
	decreaseCart() {
		if(this.countCart > 1) this.countCart = this.countCart - 1;
	}

	increaseCart() {		
		let q = this.product.quantity;
		let s = this.product.sold;
		let r = q - s;
		if(this.countCart < r) this.countCart = this.countCart + 1;
	}
}
