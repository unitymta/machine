import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service/service.service';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
	
	id: string = '';
	products: Array<[]>;

	constructor(private productService: ServiceService, private router: Router, private authService: AuthService) { }

	ngOnInit() {
		this.getProducts();
		this.id = localStorage.getItem('token');
	}

	getProducts(): void {
		this.productService.getProducts().subscribe(product => {
			this.products = product;
		});
	}

	deleteProduct(product: any): void {
		this.products = this.products.filter( p => p !== product);
		this.productService.deleteProduct(product).subscribe();
	}

	logout(){
		this.authService.logout();
		this.router.navigate(['/login']);
	}
}
