import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service/service.service';
import { ActivatedRoute } from '@angular/router';
import { log } from 'util';

@Component({
    selector: 'app-main-category',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainCategoryComponent implements OnInit {
    products: Array<[]> = [];
    productsCount: Array<[]> = [];
    page: number = 1;    

    constructor(private productService: ServiceService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.productService.getProducts().subscribe(products => this.productsCount = products);
        this.getProducts();
    }

    getProducts(): void {
        let temp: any = this.route.snapshot.paramMap.get('code');        
        this.productService.getProducts().subscribe(products => {
            switch (temp) {
                case '4x4':
                    let t: any = products.filter(value => {
                        let r: any = value["category"];
                        return r.trim().toLowerCase().replace(' ', '-') === '4x4';
                    });
                    this.products = t;
                    break;
                case 'accessories':
                    let t1: any = products.filter(value => {
                        let r: any = value["category"];
                        return r.trim().toLowerCase().replace(' ', '-') === 'accessories';
                    });
                    this.products = t1;
                    break;
                case 'electrical':
                    let t2: any = products.filter(value => {
                        let r: any = value["category"];
                        return r.trim().toLowerCase().replace(' ', '-') === 'electrical';
                    });
                    this.products = t2;
                    break;

                case 'replacement-parts':
                    let t3: any = products.filter(value => {
                        let r: any = value["category"];
                        return r.trim().toLowerCase().replace(' ', '-') === 'replacement-parts';
                    });
                    this.products = t3;
                    break;
                default:
                    this.products = products;
                    break;
            }
        });
    }

    countCategories(typeCategory: string = '', countCate: number = 0) {
        this.productsCount.map((value: any) => {
            let temp: any = value["category"].toString().toLowerCase();
            typeCategory = typeCategory.toString().toLowerCase();
            if (typeCategory === temp) {
                countCate = countCate + 1;
                return countCate;
            }
        });
        return countCate;
    }

    countBrands(typeBrand: string = '', countBrand: number = 0) {
        this.productsCount.map((value: any) => {
            let temp = value["brand"].toString().toLowerCase();
            typeBrand = typeBrand.toString().toLowerCase();
            if (typeBrand === temp) {
                countBrand = countBrand + 1;
            }
        });
        return countBrand;
    }

    countModels(typeModel: string = '', countModel: number = 0) {
        this.productsCount.map((value: any) => {
            let temp = value["model"].toString().toLowerCase();
            typeModel = typeModel.toString().toLowerCase();
            if (typeModel === temp) {
                countModel = countModel + 1;
            }
        });
        return countModel;
    }

    countPrices(prevPrice: number = 0, nextPrice: number = 0, countPrice: number = 0) {
        this.productsCount.map((value) => {
            let temp = value["newPrice"];
            if (prevPrice < temp && temp < nextPrice) {
                countPrice = countPrice + 1;
            }
        });
        return countPrice;
    }

    clickCategories(cate: string = '') {        
        this.productService.getProducts().subscribe(products => {
            switch (cate) {
                case '4x4':
                    let t: any = products.filter(value => {
                        let r: any = value["category"];
                        return r.trim().toLowerCase().replace(' ', '-') === '4x4';
                    });
                    this.products = t;
                    break;
                case 'accessories':
                    let t1: any = products.filter(value => {
                        let r: any = value["category"];
                        return r.trim().toLowerCase().replace(' ', '-') === 'accessories';
                    });
                    this.products = t1;
                    break;
                case 'electrical':
                    let t2: any = products.filter(value => {
                        let r: any = value["category"];
                        return r.trim().toLowerCase().replace(' ', '-') === 'electrical';
                    });
                    this.products = t2;
                    break;

                case 'replacement-parts':
                    let t3: any = products.filter(value => {
                        let r: any = value["category"];
                        return r.trim().toLowerCase().replace(' ', '-') === 'replacement-parts';
                    });
                    this.products = t3;
                    break;
                default:
                    this.products = products;
                    break;
            }
        });
    }

}
