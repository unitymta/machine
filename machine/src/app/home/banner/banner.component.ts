import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-banner',
	templateUrl: './banner.component.html',
	styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
	imageArr = ['../../../assets/images/static/banner-1.jpg',
				'../../../assets/images/static/banner-2.jpg',
				'../../../assets/images/static/banner-3.jpg']
	images = this.imageArr.map(value => value);

	constructor() { }

	ngOnInit() {		
	}

}
