import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, tap, catchError} from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
	providedIn: 'root'
})
export class ServiceService {

	constructor(private http: HttpClient) { }

	private productUrl = 'api/product';  // URL to web api

	getProducts(): Observable<[]> {
		return this.http.get<[]>(this.productUrl);
	}

	deleteProduct(product: any): Observable<any> {
        const id = typeof product === 'number' ? product : product['id'];
        const url = `${this.productUrl}/${id}`;
        return this.http.delete<any>(url, httpOptions);
	}	
	
	updateProduct(product: any): Observable<any>{
		return this.http.put(this.productUrl, product, httpOptions);
	}

	searchProduct(term: string): Observable<any>{
		if(!term.trim()){
			return of();
		}
		return this.http.get<[]>(`${this.productUrl}/?name=${term}`)
	}
}
