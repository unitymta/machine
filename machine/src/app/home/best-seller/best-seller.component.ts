import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service/service.service';

@Component({
    selector: 'app-best-seller',
    templateUrl: './best-seller.component.html',
    styleUrls: ['./best-seller.component.scss']
})
export class BestSellerComponent implements OnInit {
    products: [] = [];

    constructor(private productService: ServiceService) { }

    ngOnInit() {
        this.getProducts();
    }

    getProducts() {
        this.productService.getProducts().subscribe(products => {
            products.sort((a, b) => {
                return a["sold"] - b["sold"];
            });
            products.reverse();
            return this.products = products;
        });
    }
}
