import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './service/in-memory-data.service';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FooterComponent } from './footer/footer.component';
import { BannerComponent } from './home/banner/banner.component';
import { FeaturedComponent } from './home/featured/featured.component';
import { TagComponent } from './home/tag/tag.component';
import { NewArrivalComponent } from './home/new-arrival/new-arrival.component';
import { BestSellerComponent } from './home/best-seller/best-seller.component';
import { RecentyleViewedComponent } from './home/recentyle-viewed/recentyle-viewed.component';
import { AppRoutingModule } from './app-routing.module';
import { MainCategoryComponent } from './category/main/main.component';
import { MainHomeComponent } from './home/main/main.component';
import { MainDetailComponent } from './detail/main/main.component';
import { ServiceService } from './service/service.service';
import { AuthGuard } from './auth.guard'
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailComponent } from './dashboard/detail/detail.component';
import { DashboardSearchComponent } from './dashboard/dashboard-search/dashboard-search.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        NavigationComponent,
        FooterComponent,
        BannerComponent,
        FeaturedComponent,
        TagComponent,
        NewArrivalComponent,
        BestSellerComponent,
        RecentyleViewedComponent,
        MainHomeComponent,
        MainCategoryComponent,
        MainDetailComponent,
        LoginComponent,
        DashboardComponent,
        DetailComponent,
        DashboardSearchComponent
    ],
    imports: [
        BrowserModule,
        NgbModule,
        AppRoutingModule,
        HttpClientModule,
        NgxPaginationModule,
        HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {dataEncapsulation: false}),
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [AuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule { }
