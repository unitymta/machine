import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MainHomeComponent } from './home/main/main.component';
import { MainCategoryComponent } from './category/main/main.component';
import { MainDetailComponent } from './detail/main/main.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DetailComponent } from './dashboard/detail/detail.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
	{ path: '', component: MainHomeComponent },
	{ path: 'category', component: MainCategoryComponent },
	{ path: 'category/:code', component: MainCategoryComponent },
	// { path:'category/find?category=:code', component: MainCategoryComponent, pathMatch: 'full' },
	{ path: 'detail/:code', component: MainDetailComponent },
	{ path: 'login', component: LoginComponent },
	{ path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
	{ path: 'dashboard/detail/:code', component: DetailComponent }
]

@NgModule({
	declarations: [],
	exports: [RouterModule],
	imports: [
		CommonModule,
		RouterModule.forRoot(routes)
	],
	providers: [AuthGuard]
})
export class AppRoutingModule { }
