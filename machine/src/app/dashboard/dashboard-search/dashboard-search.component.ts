import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ServiceService } from '../../service/service.service';

@Component({
	selector: 'app-dashboard-search',
	templateUrl: './dashboard-search.component.html',
	styleUrls: ['./dashboard-search.component.scss']
})
export class DashboardSearchComponent implements OnInit {

	products$: Observable<[]>;
	private searchTerm = new Subject<string>();

	constructor(private productService: ServiceService) { }

	ngOnInit() {
		this.products$ = this.searchTerm.pipe(
			debounceTime(300),
			distinctUntilChanged(),
			switchMap((term:string) => this.productService.searchProduct(term)),
		);
	}

	searchProduct(term: string): void{
		this.searchTerm.next(term);
	}

}
